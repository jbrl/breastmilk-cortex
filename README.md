# Breastmilk exposure and cortical imaging features

Code to reproduce the analyses presented in: "Breast milk exposure is associated with cortical maturation in preterm infants".

Repository authors: Kadi Vaher (kadi.vaher@ed.ac.uk), Paola Galdi (paola.galdi@gmail.com)

The repository consists of the following folders:
- **scripts/** containing R markdown and script files for creating the master dataframe, demographic and clinical variable tables, and statistical analyses 
- **results/** containing output model results, figures and html reports
