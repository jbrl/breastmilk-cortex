---
title: "`r subdir_name`"
author: "Kadi Vaher"
date: "`r Sys.time()`"
output: html_document
---

```{r setup, include=F, message=F}
subdir_name <- "SIMD_perinatal"

# load packages
library(tidyverse)
library(magrittr)
library(here)
library(glue)

# set paths
knitr::opts_knit$set(root.dir = ".", aliases = c(h = "fig.height", w = "fig.width", ow = "out.width"))
knitr::opts_chunk$set(
  dev = c("png", "pdf"),
  fig.path = here("results", "figures", glue("{subdir_name}/")), dpi = 300
)
theme_set(theme_light()) # global option for ggplot2
```

```{r knit, echo=F, eval=FALSE}
rmarkdown::render(
  input = here("scripts", str_c(subdir_name, ".Rmd")),
  output_dir = here("results", "reports")
)
```

# Load data and wrangling

These files are in a long format so I will take out each timepoint SIMD and other data included here to have them as separate files for each participant

```{r, echo=FALSE}
simd_all <- read.csv(here("raw_data", "EdinburghBirthCohort-Kadiperinatalsimd_DATA_2022-07-22_1058.csv"))

simd_record_ids <- simd_all %>% select(record_id)
simd_record_ids <- distinct(simd_record_ids)

simd_antenatal <- simd_all %>% subset(redcap_event_name=="antenatal_arm_1") %>% select(record_id, simd_quintile)
simd_antenatal <- simd_antenatal %>% rename(simd_antenatal = simd_quintile)

simd_birth <- simd_all %>% subset(redcap_event_name=="birth_arm_1") %>% select(record_id, simd_quintile)
simd_birth <- simd_birth %>% rename(simd_birth = simd_quintile)

simd_neonatal <- simd_all %>% subset(redcap_event_name=="neonatal_arm_1") %>% select(record_id, simd_quintile)
simd_neonatal <- simd_neonatal %>% rename(simd_neonatal = simd_quintile)

```

Now I will merge them together:

```{r message=FALSE, warning=FALSE, results='hide'}
simd <- merge(simd_record_ids, simd_antenatal, by="record_id", all.x = T)
simd <- merge(simd, simd_birth, by="record_id", all.x = T)
simd <- merge(simd, simd_neonatal, by="record_id", all.x = T)

```

Create a new variable called simd_earliest_REDCap that would take the earliest simd we have recorded from antenatal/birth/neonatal

```{r message=FALSE, warning=FALSE, results='hide'}
simd <- simd %>% mutate(SIMD_perinatal = ifelse(is.na(simd_antenatal), simd_birth, simd_antenatal))
simd <- simd %>% mutate(SIMD_perinatal = ifelse(is.na(SIMD_perinatal), simd_neonatal, SIMD_perinatal))

```

Write csv file

```{r message=FALSE, warning=FALSE, results='hide'}

write.csv(simd, here("processed_data", "simd_perinatal_22072022.csv"), row.names = F)

```
